# Aether SASS

[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_cdn-kde-org-aether-devel)](https://binary-factory.kde.org/job/Website_cdn-kde-org-aether-devel/)

KDE has a lot of websites using each their own variation of aether, the goal of this repository
is to create a set of sass modules that are used to generate multiple css files. One for each
aether variations.

For Hugo-based websites, please check the "hugo" branch of this repo.

## Installation

You need yarn installed (a recent version).

```bash
git clone git@invent.kde.org/websites/aether-sass.git
yarn install
```

## Building (dev)

This will rebuild the css files after each change. This is fast.

```bash
LOCAL=true yarn encore dev --watch
```

or you can manual rebuild the css files. This is slow.

```bash
LOCAL=true yarn encore dev
```

You can then browse the examples in `example` to see your changed :)

## Building (production)

```
yarn encore production
```

## Code style

New code should be compatible with these conventions:
- **Indentation**: 2 spaces
- One line between each CSS declaration
- One SASS module for each logical component.
